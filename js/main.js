"use strict";
 
const arr = ['hello', 'world', 23, '23', null,45];

function filterBy(arr,arg,newArr){
    newArr = [];
    for(let i = 0;i < arr.length; i++){
        if(typeof arr[i] !== arg){
            newArr.push(arr[i]);
        }
    }
    return newArr;
}

console.log(filterBy(arr,"string"));
